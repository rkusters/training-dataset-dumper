from dataclasses import dataclass

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

from .base import BaseBlock


@dataclass
class TruthLabelling(BaseBlock):
    """
    Re-run the FTAG truth labelling.

    Parameters
    ----------
    suffix : str
        Suffix to append to the truth labelling variables.
    """
    suffix: str

    def to_ca(self):
        ca = ComponentAccumulator()
        trackTruthOriginTool = CompFactory.InDet.InDetTrackTruthOriginTool(
            isFullPileUpTruth=False
        )
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TruthParticleDecoratorAlg(
                "TruthParticleDecoratorAlg",
                trackTruthOriginTool=trackTruthOriginTool,
                ftagTruthOriginLabel="ftagTruthOriginLabel" + self.suffix,
                ftagTruthVertexIndex="ftagTruthVertexIndex" + self.suffix,
                ftagTruthTypeLabel="ftagTruthTypeLabel" + self.suffix,
                ftagTruthSourceLabel="ftagTruthSourceLabel" + self.suffix,
                ftagTruthParentBarcode="ftagTruthParentBarcode" + self.suffix,
            )
        )
        ca.addEventAlgo(
            CompFactory.FlavorTagDiscriminants.TrackTruthDecoratorAlg(
                "TrackTruthDecoratorAlg",
                trackTruthOriginTool=trackTruthOriginTool,
                acc_ftagTruthVertexIndex="ftagTruthVertexIndex" + self.suffix,
                acc_ftagTruthTypeLabel="ftagTruthTypeLabel" + self.suffix,
                acc_ftagTruthSourceLabel="ftagTruthSourceLabel" + self.suffix,
                acc_ftagTruthParentBarcode="ftagTruthParentBarcode" + self.suffix,
                dec_ftagTruthOriginLabel="ftagTruthOriginLabel" + self.suffix,
                dec_ftagTruthVertexIndex="ftagTruthVertexIndex" + self.suffix,
                dec_ftagTruthTypeLabel="ftagTruthTypeLabel" + self.suffix,
                dec_ftagTruthSourceLabel="ftagTruthSourceLabel" + self.suffix,
                dec_ftagTruthBarcode="ftagTruthBarcode" + self.suffix,
                dec_ftagTruthParentBarcode="ftagTruthParentBarcode" + self.suffix,
            )
        )
        return ca
