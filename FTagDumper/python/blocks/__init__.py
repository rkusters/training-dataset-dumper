from .TruthLabelling import TruthLabelling

__all__ = [
    "TruthLabelling",
]
