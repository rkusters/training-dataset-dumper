from abc import ABC, abstractmethod
from dataclasses import dataclass
from typing import Any

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator


@dataclass
class BaseBlock(ABC):
    dumper_config: dict[str, Any]
    cfgFlags: Any

    @abstractmethod
    def to_ca(self) -> ComponentAccumulator:
        pass
